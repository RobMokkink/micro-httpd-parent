FROM registry.access.redhat.com/ubi8/ubi-minimal:8.0

MAINTAINER Rob Mokkink rob@mokkinksystems.com

ENV DOCROOT="/var/www/html"

RUN microdnf install httpd -y && \
    microdnf clean all -y

ONBUILD COPY src/ ${DOCROOT}/

EXPOSE 80

RUN rm -rf /run/httpd && mkdir /run/httpd

USER root

CMD ["httpd", "-D", "FOREGROUND"]

